# 空间计量专题研讨班课程大纲

&emsp;

> &#x2728; [连享会·听课建议](https://www.bilibili.com/video/BV1jv411q77T) ( [Bilibili](https://space.bilibili.com/546535876) )

> &#x1F449;  常见问题解答和每日 FAQs 在这里 【[Wiki](https://gitee.com/arlionn/SP/wikis)】     
     
> &#x1F34F; 点击右上角【[Fork](https://gitee.com/arlionn/SP#)】按钮，把本仓库复制到你的码云页面下。

&emsp;

> ### Update: `2021.06.08`，&#x1F449;  [点击查看](https://file.lianxh.cn/KC/lianxh_SP.pdf) **PDF 课程大纲**

&emsp; 

---

**目录**

1. 课程概览    
2. 主讲嘉宾简介    
3. 课程详情 (Part A)    
4. 课程详情 (Part B)   
5. 报名信息   
6. 助教招聘 ⚽   

---


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxh_SP2021_005.png) 


&emsp; 


## 1. 课程概览

- &#x231A; **时间：** 2021 年 8 月 21-24 日 (周六-周二, 四天)
- &#x1F349; **地点：** 网络直播
- &#x1F303; **主讲嘉宾**：杨海生 (中山大学)；范巧 (兰州大学)
- &#x1F4D9; **授课方式：**
  - 每天 6 小时 (9:00-12:00；14:30-17:30) + 半小时答疑
  - 讲义电子版于开课前一周发送，内含软件安装指南
  - 软件：Stata + Matlab (R2019a, R2020a)
- &#x26A1; **报名连接：** <http://junquan18903405450.mikecrm.com/EmGijiK>
- &#x26F3;  **课程主页：** <https://gitee.com/lianxh/SP> &#x2B55; [PDF课纲](https://file.lianxh.cn/KC/lianxh_SP.pdf) 
- &#x26BD; **助教招聘**：https://www.wjx.top/jq/90823024.aspx

&emsp;

## 2. 主讲嘉宾简介

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/杨海生100.jpg)

[杨海生](https://lingnan.sysu.edu.cn/faculty/yanghaisheng)，中山大学岭南学院经济学系副教授，主要研究领域为空间计量经济学理论与应用、实证金融。在 Emerging Markets Review、 Economic Geography、Ecological Economics、《经济研究》、《管理世界》、《经济学（季刊）》、《管理科学学报》、《金融研究》、《会计研究》、《世界经济》等学术刊物上发表多篇论文，主持和参与多项国家自然科学基金、广东省自然科学基金等课题研究。

&emsp; 

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E8%8C%83%E5%B7%A7%E5%B7%A5%E4%BD%9C%E7%85%A7110.png)

**范巧**，兰州大学经济学院博士研究生，教授、硕士生导师，研究方向为区域与城市经济发展战略，曾任 Texas Tech University 访问学者、中国人民大学高级访问学者、重庆市社科联理论研究室副主任；全国投入产出与大数据研究会常务理事、重庆市数量经济学会理事、重庆市商务委评标专家；在经济科学出版社等出版专著 3 部，在《数量经济技术经济研究》、Applied Mathematics and Nonlinear Sciences 等发表学术论文 40 余篇，主持纵向科研项目十余项；「小范空间计量工作室」公众号创办人。

&emsp;

## 3. 课程详情 (Part A)

### 3.1 课程介绍

**Part A** 将通过为期两天的讲解，帮助学员们了解并掌握空间相关性的产生缘由、基本的空间计量模型的设定、估计以及相应的实证应用。

具体而言，我们将回顾各类空间计量模型的由来、发展及演变历程。不仅会介绍不同空间模型的设定及其相应的估计方法，也将讨论使用这些模型的设定思想和结果的经济含义解读。课程的关注重点在于空间计量模型的应用，我们将利用经典文献的数据集对不同的空间计量模型进行演示，并提供分析结果所使用的 **Stata+Matlab** 程序，以便学员们能够这些方法迁移到自己的论文中。应用思路的扩展也很重要，为此，我们还会结合最新 Top 期刊文献，介绍空间计量模型的各类拓展，及其在金融、国际贸易、环境经济学、财政以及区域经济学等领域中的应用，以启发大家找到合适的论文选题。

> 各个专题的具体介绍如下：

**第一讲** 首先回顾三代空间计量模型的设定思想和发展演变历程，以及不同空间模型之间的差异，以便各位学员能够合理设定和筛选模型；进而讲解空间计量模型中的三种主要估计方法：MLE、IV 和 GMM 的区别与联系。

**第二讲** 将在第一讲的基础上引导学员对空间权重矩阵的设定进行扩展，并讲解如何选择空间模型 （即对 SEM，SAR，SAC，SLX，SDM，SDEM 模型设定的含义及检验进行剖析)。其次，课程将以 Anselin ([1988](https://www.jianguoyun.com/p/DWWpSZAQtKiFCBj20rwD)) 对犯罪率的研究和 Stata manual 中的数据为例，讲解如何利用 Stata 和 Matlab 对截面空间模型进行估计和检验。最后，介绍实证结果的呈现方式和参数的经济含义解读。

**第三讲** 介绍高阶空间滞后模型及空间联立方程。对于前者，重点介绍包含高阶空间滞后项的空间计量模型的空间权重设定、IV 及 GMM 估计方法及 Matlab + Stata 实现；对于后者，首先基于 Kelejian and Prucha ([2004](https://www.jianguoyun.com/p/DRxqT1IQtKiFCBiMnPcD)) 的论文帮助学员们理解如何在 Stata 中对空间联立方程进行设定和估计，进而结合 Chen, et al. ([2015](https://www.jianguoyun.com/p/DQezotAQtKiFCBjwlPcD)) 的文章讲解该模型如何在金融、国际贸易、财政及区域经济学领域进行拓展。

**第四讲** 针对学员们普遍关心 (也是目前空间计量应用中最为常见的数据结构) 的面板数据，本课程将引导学员使用 Matlab 和 Stata 两种软件对静态空间面板模型进行估计和解读，辅以多个案例数据剖析静态空间面板应用过程中的主要陷阱。

在课程的最后一部分，我们将通过一个热门话题 —— **同行效应** 的深入剖析来理解经典空间计量模型在金融领域特别是实证金融中的应用，以便各位了解从「讲故事」到「建模型」这一过程中可能遇到的各种障碍和解决方法。首先，我们从 Peer Effect 的界定出发，结合数据可得性，介绍文献中各类空间权重矩阵的构造方法，这是「**故事 &rarr; 模型**」的第一步；其次，介绍 Peer Effect 研究中公司之间交互影响的微观理论基础，把空间计量模型的应用扩展到实证金融的诸多领域；最后，介绍 Peer Effect 的识别问题和参数估计 Stata + Matlab 实现。


### 3.2 课程大纲

> #### **第 1 讲** &ensp; 何谓空间计量？ (3 小时)
- 空间计量模型的由来与发展
- 空间权重矩阵的设定与实现
- 空间计量模型的估计方法
- 论文解读：
  - Elhorst ([2014](https://www.jianguoyun.com/p/DRlTNMwQtKiFCBiXnPcD))
  - Lee ([2007](https://www.jianguoyun.com/p/Dfn49pgQtKiFCBihnPcD))
  - LeSage and Pace ([2009](https://www.jianguoyun.com/p/DZWmPacQtKiFCBiknPcD))
  - Aquaro, Bailey and Pesaran ([2021](https://www.jianguoyun.com/p/DS-tCgoQtKiFCBiZk_cD))
  - Keane and Neal ([2020](https://www.jianguoyun.com/p/DTBst8oQtKiFCBiuk_cD))

> #### **第 2 讲** &ensp; 截面空间计量模型的扩展 (3 小时)

- 主要的空间计量模型
- 模型的选择与检验
- 模型的展示与解释
- 如何克服空间权重矩阵内生性
- 论文解读：Matlab + Stata 实现 
  - Qu and Lee ([2015](https://www.jianguoyun.com/p/DREVkx8QtKiFCBjlk_cD)) 
  - Qu, Wang and Lee ([2016](https://www.jianguoyun.com/p/DePTVroQtKiFCBjak_cD))
- 空间计量模型实操：Anselin ([1988](https://www.jianguoyun.com/p/DWWpSZAQtKiFCBj20rwD))，犯罪率和家庭收入的截面空间实证模型 (Matlab+Stata) 
–	Stata manual 空间工具箱应用

> #### **第 3 讲** &ensp; 高阶空间滞后和空间联立方程模型 (3 小时)
- 高阶空间滞后模型 (1.5 小时)
  - 模型的由来 (Case et al., [1993](https://www.jianguoyun.com/p/De7LdoYQtKiFCBixnPcD)) 
  - 模型的估计 (IV 和 GMM) Lee and Liu ([2010](https://www.jianguoyun.com/p/DUxzyb4QtKiFCBi7nPcD))
  - Matlab + Stata 实现及应用
- 空间联立方程模型 (1.5 小时)
  - 空间联立方程的简介：Chen et al. ([2015](https://www.jianguoyun.com/p/DQezotAQtKiFCBjwlPcD))
  - 空间联立方程的估计：Kelejian & Prucha ([2004](https://www.jianguoyun.com/p/DRxqT1IQtKiFCBiMnPcD))
  - 空间联立方程的 Stata 实现及应用

> #### **第 4 讲** &ensp; 空间面板和同群效应     

- 空间面板数据模型简介
- Stata manual 空间工具箱应用
  - 论文解读：Pesaran and Yang ([2020](https://www.jianguoyun.com/p/DWDPCvwQtKiFCBi_nPcD))
- 应用：同群效应 (Peer Effect) 实例剖析
  - Peer Effect 与空间计量：由来与设定
  - 资产定价中的 Peer Effect (Stata+Matlab)：Pirinsky & Wang ([2006](https://www.jianguoyun.com/p/DdMEtDcQtKiFCBjNnPcD))； Parsons et al. ([2020](https://www.jianguoyun.com/p/DV3TbwcQtKiFCBijmPcD))
  - 公司决策中的 Peer Effect (Stata)：Foucault & Fresard ([2014](https://www.jianguoyun.com/p/DdHci88QtKiFCBjfnPcD))；Seo et al.([2021](https://www.jianguoyun.com/p/DV6ijgQQtKiFCBiHmfcD))
  - Peer Effect 的识别拓展 (工具变量的选择、谁向谁学习或模仿)：Leary & Roberts ([2014](https://www.jianguoyun.com/p/DW8ytzsQtKiFCBjgnPcD))；连玉君等 ([2020](https://www.jianguoyun.com/p/DTxGRUoQtKiFCBilmfcD))
  - 其它：Blasques et al. ([2016](https://www.jianguoyun.com/p/DV4zHEUQtKiFCBjpnPcD))；Hauzenberger and Pfarrhofer ([2021](https://www.jianguoyun.com/p/DZJ1EgAQtKiFCBiGnfcD)) 



### 3.3 参考文献

> **打包下载：** [点击查看所有文献](https://www.jianguoyun.com/p/DfRQsIkQtKiFCBiYm_cD)  

- **Anselin**, Luc. **1988**. Spatial Econometrics: Methods and Models. [-Link-](https://www.jianguoyun.com/p/DWWpSZAQtKiFCBj20rwD) 
- **Aquaro** M., Bailey N., Pesaran M. H., **2021**, Estimation and Inference for Spatial Models with Heterogeneous Coefficients: An Application to U.S. House Prices[J]. Journal of Applied Econometrics 36 (1): 18–44. [-PDF-](https://www.jianguoyun.com/p/DS-tCgoQtKiFCBiZk_cD)
- **Blasques** F, Koopman S J, Lucas A, et al. **2016**, Spillover dynamics for systemic risk measurement using spatial financial time series models[J]. Journal of Econometrics, 195(2): 211-223. [-PDF-](https://www.jianguoyun.com/p/DV4zHEUQtKiFCBjpnPcD)
- **Case** A. C. , Rosen H. S., Hines J. R., **1993**, Budget spillovers and fiscal policy interdependence: Evidence from the states[J]. Journal of Public Economics, 52 (3): 285-307. [-PDF-](https://www.jianguoyun.com/p/De7LdoYQtKiFCBixnPcD)
- **Chen** Shaoling, Wang Susheng, Yang Haisheng, **2015**, Spatial Competition and Interdependence in Strategic Decisions: Empirical Evidence from Franchising[J]. Economic Geography, 91 (2): 165-204. [-PDF-](https://www.jianguoyun.com/p/DQezotAQtKiFCBjwlPcD)
- **Elhorst** J. P., **2014**, Spatial econometrics: from cross-sectional data to spatial panels [M], Heidelberg, New York, Dordrecht, London: Springer. [-PDF-](https://www.jianguoyun.com/p/DRlTNMwQtKiFCBiXnPcD)
- **Foucault** T., Fresard L., **2014**, Learning from Peers' Stock Prices and Corporate Investment[J]. Journal of Financial Economics, 111 (3): 554-577. [-PDF-](https://www.jianguoyun.com/p/DdHci88QtKiFCBjfnPcD)
- **Hauzenberger**, Niko, and Michael Pfarrhofer. **2021**. Bayesian State‐space Modeling for Analyzing Heterogeneous Network Effects of US Monetary Policy. The Scandinavian Journal of Economics, forthcoming. [-PDF-](https://www.jianguoyun.com/p/DZJ1EgAQtKiFCBiGnfcD)
- **Keane** M., Neal. T, **2020**, Climate Change and U.S. Agriculture: Accounting for Multidimensional Slope Heterogeneity in Panel Data[J], Quantitative Economics, 11 (4): 1391–1429. [-PDF-](https://www.jianguoyun.com/p/DTBst8oQtKiFCBiuk_cD), [-Link-](https://qeconomics.org/ojs/index.php/qe/article/view/1382)
- **Kelejian** H. H., Prucha I. R., **2010**, Specification and estimation of spatial autoregressive models with autoregressive and heteroskedastic disturbances[J]. Journal of Econometrics, 157 (1): 53-67. [-PDF-](https://www.jianguoyun.com/p/DY_VZXEQh8O_CBi7xbwD)
- **Leary** M. T., Roberts M. R., **2014**, Do Peer Firms Affect Corporate Financial Policy?[J]. Journal of Finance, 69 (1): 139–178. [-PDF-](https://www.jianguoyun.com/p/DW8ytzsQtKiFCBjgnPcD)
- **Lee** Lung-fei, Jihai Yu, **2010**, Estimation of spatial autoregressive panel data models with fixed effects[J]. Journal of Econometrics,154 (2): 165-185. [-PDF-](https://www.jianguoyun.com/p/DUxzyb4QtKiFCBi7nPcD)
- **Lee** Lung-Fei, **2007**, GMM and 2SLS estimation of mixed regressive, spatial autoregressive models[J]. Journal of Econometrics, 137 (2): 489-514. [-PDF-](https://www.jianguoyun.com/p/Dfn49pgQtKiFCBihnPcD)
- **Lee** Lung-fei, Xiaodong Liu, **2010**, Efficient GMM estimation of high order spatial autoregressive models with autoregressive disturbances[J]. Econometric Theory, 26 (1): 44. [-PDF-](https://www.jianguoyun.com/p/DUxzyb4QtKiFCBi7nPcD)
- **LeSage** J. P., Pace R. K., **2009**, Introduction to spatial econometrics [M], New York: CRC Press Taylor & Francis Group. [-PDF-](https://www.jianguoyun.com/p/DZWmPacQtKiFCBiknPcD)
- **Parsons**, C. A., R. Sabbatucci, and S. Titman. **2020**. “Geographic Lead-Lag Effects.” Review of Financial Studies 33 (10): 4721–70. [-PDF-]()
- **Pesaran** M. H., Yang C. F., **2020**, Econometric Analysis of Production Networks with Dominant Units[J]. Journal of Econometrics. [-PDF-](https://www.jianguoyun.com/p/DWDPCvwQtKiFCBi_nPcD)
- **Pirinsky** C., Wang Q, **2006**, Does Corporate Headquarters Location Matter for Stock Returns?[J]. The Journal of Finance, 61 (4): 1991-2015. [-PDF-](https://www.jianguoyun.com/p/DdMEtDcQtKiFCBjNnPcD)
- **Qu**, Xi, and Lung-fei Lee. **2015**. Estimating a Spatial Autoregressive Model with an Endogenous Spatial Weight Matrix. Journal of Econometrics 184 (2): 209–32. [-PDF-](https://www.jianguoyun.com/p/DREVkx8QtKiFCBjlk_cD)
- **Qu**, Xi, Xiaoliang Wang, and Lung‐fei Lee. **2016**. Instrumental Variable Estimation of a Spatial Dynamic Panel Model with Endogenous Spatial Weights When T Is Small. Econometrics Journal 19 (3): 261–90. [-PDF-](https://www.jianguoyun.com/p/DePTVroQtKiFCBjak_cD), [-Link-](https://academic.oup.com/ectj/article-abstract/19/3/261/5056408)
- **Seo**, Hojun. **2021**. Peer Effects in Corporate Disclosure Decisions. Journal of Accounting and Economics 71 (1): 101364. [-PDF-](https://www.jianguoyun.com/p/DV6ijgQQtKiFCBiHmfcD)
- **连玉君**, 彭镇, 蔡菁, 杨海生. 经济周期下资本结构同群效应研究. 会计研究, **2020**, (11): 85-97. [-PDF-](https://www.jianguoyun.com/p/DTxGRUoQtKiFCBilmfcD)

&emsp;

## 4. 课程详情 (Part B)

### 4.1 课程介绍

**Part B** 将通过为期两天的讲解，帮助学员了解并掌握动态面板空间计量模型、空间双重差分模型、空间局部分析模型、时空地理加权回归模型及其 MATLAB 软件实现。具体来说，我们将回顾面板数据空间计量模型的一般建模范式，并在此基础上阐释动态和非平稳空间面板模型的建模框架；我们还将纳入空间双重差分模型，解析纳入空间因素后政策评价模型的演化，并阐释其平行趋势和安慰剂检验等特定问题；同时，一些最新的局部分析模型也将被纳入课程，包括地理加权主成分分析、空间滤波模型、适应面板数据的时空地理加权回归模型等，以解析解释变量参数的空间漂移性。本课程将嵌入主讲嘉宾近期的理论研究成果，并提供完整的、简洁的 MATLAB 工具箱及各种模型代码，方便学员建模使用。此外，课程所有代码将由主讲嘉宾重新调试和审定。

> 各个专题的具体介绍如下：

**第 5 讲** 首先，介绍MATLAB软件的安装及基本功能；在此基础上介绍软件界面及功能、数据录入及基本处理、数据调用、存储及作图等；随后，将讲解空间计量工具箱的安装及调用、脚本函数及运行函数编写及修改、空间计量模型代码包调用及程序修改等。

**第 6 讲** 首先，回顾静态面板数据空间计量模型的建模范式，包括模型设定、参数估计、模型优选及参数效应分解等；随后，介绍动态面板空间计量模型的基本设定，并结合动态 SDM 面板空间模型、动态 SEM 面板空间模型，阐释动态面板空间计量模型的建模框架；最后，还将讨论动态面板空间计量模型的非平稳性及其误差修正模型。

**第 7 讲** 首先，阐释空间双重差分模型的基本模型设定，并解析其参数估计、政策效应分解等细节，包括全局政策效应分解与局部政策效应分解等；同时，还将阐释空间双重差分模型的平行趋势检验和安慰剂检验等特定问题。

**第 8 讲** 首先，解释地理加权回归模型的建模过程，并在此基础上，结合主讲嘉宾对地理加权回归模型约为两年半时间的跟踪研究，阐释地理加权回归模型方法及其新进展；随后，在基于全息映射的时空权重矩阵设计与优选以及适应面板数据建模需求等基础上，阐释面板时空地理加权回归模型的基本建模范式。


 

### 4.2 课程大纲

> #### **第 5 讲** &ensp; MATLAB 软件解析及编程基础 (3 小时)
- MATLAB软件的安装及基本功能
- 数据处理、调用及作图
- 空间计量工具箱的安装及调用：JPLV7、Toolbox_FQ_2020
- 脚本函数、运行函数的编写及修改
- 空间计量模型代码包调用及程序修改

> #### **第 6 讲** &ensp; 面板、动态面板与非平稳空间计量模型 (3 小时)

- 静态空间面板模型的建模回顾
- 动态空间面板模型的基本设定
- 动态面板 SDM 模型：模型设定、DGP 过程、参数估计与效应分解
- 动态面板 SEM 模型：模型设定、DGP 过程、参数估计与效应分解
- 非平稳动态面板 SDM 模型及其误差修正模型
- 论文解读：
  - Ertur & Koch ([2007](https://www.jianguoyun.com/p/DX-fcC4QtKiFCBjmtLwD))
  - Elhorst et al. ([2010](https://www.jianguoyun.com/p/DWiCRjMQtKiFCBjZtLwD))
  - Elhorst ([2005](https://www.jianguoyun.com/p/DVtmy4sQtKiFCBjltLwD))
  - Yang et al. ([2006](https://www.jianguoyun.com/p/DWWz82gQtKiFCBj5tLwD))

> #### **第 7 讲** &ensp; 双重差分空间计量模型 (SDID) (3 小时）
- SDID 模型的源起及主要分析范式
- SDID 模型的设定、估计与政策效应分解
- 非线性 SDID 模型的设定、估计与政策效应分解
- SDID 模型的平行趋势检验与安慰剂检验
- 论文解读：
  - Chagas et al. ([2016](https://www.jianguoyun.com/p/DYyjzgQQtKiFCBjVtLwD))
  - Kolak & Anselin ([2020](https://www.jianguoyun.com/p/DVUPqx8QtKiFCBj1tLwD))

> #### **第 8 讲** &ensp;  面板时空地理加权回归模型 (PGTWR) (3 小时)
- 地理加权回归模型及 GWR、MGWR 软件实现
- 经典时空地理加权回归模型及其局限
- PGTWR 的基本设定
- PGTWR 时空权重矩阵的设计与优选
- PGTWR 参数估计及假设检验
- PGTWR 的稳健性分析与预测
- 论文解读：
  - Huang et al. ([2010](https://www.jianguoyun.com/p/DUfOEtwQtKiFCBj0tLwD))
  - Fotheringham et al. ([2015](https://www.jianguoyun.com/p/DdF_5IQQtKiFCBjptLwD))
  - Fotheringham et al. ([2017](https://www.jianguoyun.com/p/Dan_VagQtKiFCBjxtLwD))
  - Du et al. ([2018](https://www.jianguoyun.com/p/Dd6sJ0YQtKiFCBjYtLwD))
  - Bidanset et al. ([2018](https://www.jianguoyun.com/p/DfgTQLQQtKiFCBjSsLwD))
  - Chen & Mei ([2020](https://www.jianguoyun.com/p/DYCIFVMQtKiFCBjXtLwD))
  - 范巧和郭爱君 ([2021](https://www.jianguoyun.com/p/DWUYJa4QtKiFCBjWkvcD))
 

### 4.3 参考文献

**温馨提示：** 文中链接在微信中无法生效。请点击底部<font color=DarkGreen>「阅读原文」</font>。

> **打包下载：** [点击查看所有文献](https://www.jianguoyun.com/p/DfRQsIkQtKiFCBiYm_cD)

- **LeSage** J. P., Pace R. K., **2009**, Introduction to spatial econometrics [M], New York: CRC Press Taylor & Francis Group. [-Link-](https://www.jianguoyun.com/p/DcvKR_wQtKiFCBjUtLwD)
- **Bidanset** P., et al., **2018**, Accounting for locational, temporal, and physical similarity of residential sales in mass appraisal modeling: introducing the development and application of geographically, temporally, and characteristically weighted regression, Journal of Property Tax Assessment & Administration, 14(02): 4~12. [-PDF-](https://www.jianguoyun.com/p/DfgTQLQQtKiFCBjSsLwD)
- **Chagas** A. L. S., Azzoni C. R., Almeida A. N., **2016**, A spatial difference-in-differences analysis of the impact of sugarcane production on respiratory diseases, Regional Science and Urban Economics, 59: 24~36. [-PDF-](https://www.jianguoyun.com/p/DYyjzgQQtKiFCBjVtLwD)
- **Chen** F., Mei C., **2020**, Scale-adaptive estimation of mixed geographically weighted regression models, Economic Modelling. [-PDF-](https://www.jianguoyun.com/p/DYCIFVMQtKiFCBjXtLwD)
- **Du** Z., et al., **2018**, Extending geographically and temporally weighted regression to account for both spatiotemporal heterogeneity and seasonal variations in coastal seas, Ecological Informatics, 43: 185~199. [-PDF-](https://www.jianguoyun.com/p/Dd6sJ0YQtKiFCBjYtLwD)
- **Elhorst** J. P., **2005**, Unconditional maximum likelihood estimation of linear and log-linear dynamic models for spatial panels. Geographically Analysis, 37(1): 62~83. [-PDF-](https://www.jianguoyun.com/p/DVtmy4sQtKiFCBjltLwD)
- **Elhorst** J. P., Piras G., Arbia G., **2010**, Growth and convergence in a multiregional model with space–time dynamics, Geographically Analysis, 52(03): 338~355. [-PDF-](https://www.jianguoyun.com/p/DWiCRjMQtKiFCBjZtLwD)
- **Elhorst** J. P., **2014**, Spatial econometrics: from cross-sectional data to spatial panels [M], Heidelberg, New York, Dordrecht, London: Springer. [-PDF-](https://www.jianguoyun.com/p/DbVq8w8QtKiFCBjetLwD)
- **Ertur** C., Koch W., **2007**, Growth, technological interdependence and spatial externalities: theory and evidence. Journal of Applied Econometrics, 22(6): 1033~1062. [-PDF-](https://www.jianguoyun.com/p/DX-fcC4QtKiFCBjmtLwD)
- **Fotheringham** A. S., Crespo R., Yao J., **2015**, Geographical and temporal weighted regression (GTWR), Geographical Analysis, 47: 431~452. [-PDF-](https://www.jianguoyun.com/p/DdF_5IQQtKiFCBjptLwD)
- **Fotheringham** A. S., Yang W., Kang W., **2017**, Multiscale Geographically Weighted Regression (MGWR), Annals of American Association of Geographers, 107(06): 1247~1265. [-PDF-](https://www.jianguoyun.com/p/Dan_VagQtKiFCBjxtLwD)
- **Harris** P.，Brunsdon C.，Charlton M., **2011**, Geographically weighted principal components analysis, International Journal of Geographical Information Science, 25(10): 1717~1736. [-PDF-](https://www.jianguoyun.com/p/DW6iPl0QtKiFCBjztLwD)
- **Huang** B., Wu B., Barry M., **2010**, Geographically and temporally weighted regression for modeling spatio-temporal variation in house prices, International Journal of Geographical Information Science, 24(03): 383~401. [-PDF-](https://www.jianguoyun.com/p/DUfOEtwQtKiFCBj0tLwD)
- **Kolak** M., Anselin L., **2020**, A spatial perspective on the econometrics of program evaluation, International Regional Science Review, 43(1/2): 128~153. [-PDF-](https://www.jianguoyun.com/p/DVUPqx8QtKiFCBj1tLwD)
- **Oshan** T, M., Fotheringham A. S., **2018**, A comparison of spatially varying regression coefficient estimates using geographically weighted and spatial-filter-based techniques, Geographical Analysis, 50: 53~75. [-PDF-](https://www.jianguoyun.com/p/Da03NOwQtKiFCBj4tLwD)
- **Yang** Z., Li C., Tse Y. K., **2006**, Functional form and spatial dependence in spatial panels, Economics Letters, 91(1): 138~145. [-PDF-](https://www.jianguoyun.com/p/DWWz82gQtKiFCBj5tLwD)
- **范巧**, 郭爱君. 2021. 一种新的基于全息映射的面板时空地理加权回归模型方法[J]. 数量经济技术经济研究, 38 (4): 120-138. [-PDF-](https://www.jianguoyun.com/p/DWUYJa4QtKiFCBjWkvcD)

&emsp; 

## 5. 报名信息
- **主办方：** 太原君泉教育咨询有限公司
- **标准费用**：3800 元/人 (全价)
- **优惠方案**：
  - **连享会老学员 (现场班/专题课)/团报（3人及以上）：** 9折，3420 元/人
  - **会员：** 85折，3230 元/人
  - **温馨提示：** 以上各项优惠不能叠加使用。
- **联系方式：**
  - 邮箱：[wjx004@sina.com](wjx004@sina.com)
  - 王老师：18903405450 (微信同号)
  - 李老师：‭18636102467 (微信同号)

> **特别提示：**
>
> 为保护讲师的知识产权和您的账户安全，系统会自动在您观看的视频中嵌入您的「**用户名**」信息，每个 **账号** 锁定一台设备进行观看，以切实保护您的权益。

&emsp;

> **报名链接：** [http://junquan18903405450.mikecrm.com/EmGijiK](http://junquan18903405450.mikecrm.com/EmGijiK)

> &#x23E9; 长按/扫描二维码报名：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/报名：空间计量.png)

### 缴费方式

> **方式 1：对公转账**

- 户名：太原君泉教育咨询有限公司
- 账号：35117530000023891 (山西省太原市晋商银行南中环支行)
- **温馨提示：** 对公转账时，请务必提供「**汇款人姓名-单位**」信息，以便确认。

> **方式 2：扫码支付**

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会2020暑期支付二维码180.png)

> **温馨提示：** 扫码支付后，请将「**付款记录**」截屏发给王老师-18903405450（微信同号）

&emsp;

&emsp;

<div STYLE="page-break-after: always;"></div>

## 6. 助教招聘 &#x26BD;

---

> ### 说明和要求

- **名额：** 10 名 
- **任务：**
  - **A. 课前准备**：协助完成 3 篇介绍 Stata /MATLAB  和计量经济学基础知识的文档；
  - **B. 开课前答疑**：协助学员安装课件和软件，在微信群中回答一些常见问题；
  - **C. 上课期间答疑**：针对前一天学习的内容，在微信群中答疑 (8:00-9:00，19:00-22:00)；
  - Note: 下午 5:30-6:00 的课后答疑由主讲教师负责。
- **要求：** 热心、尽职，熟悉 Stata 的基本语法和常用命令，能对常见问题进行解答和记录 (往期助教优先录用)。
- **截止时间：** 2021 年 6 月 20 日 (将于 6 月 22 日公布遴选结果于 [课程主页](https://gitee.com/lianxh/SP)，及 连享会主页 [lianxh.cn](https://www.lianxh.cn))

> **申请链接：** <https://www.wjx.top/jq/90823024.aspx>

> 扫码填写助教申请资料：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/助教招聘：空间计量.png)

> &#x26F3;   **课程主页：** <https://gitee.com/lianxh/SP>

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/横条-远山03-窄版.jpg)



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp;

## 相关课程

>### 免费公开课  

- [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟，[课程主页](https://gitee.com/arlionn/PanelData)
- [Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟.    
- [Stata 小白的取经之路](https://lianxh.duanshu.com/#/brief/course/137d1b7c7c0045e682d3cf0cb2711530) - 龙志能，时长：2 小时，[课程主页](https://gitee.com/arlionn/StataBin) 
- 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等)  


>### 最新课程-直播课   

| 专题 | 嘉宾    | 直播/回看视频    |
| --- | --- | --- |
| &#x2B50; **[最新专题](https://www.lianxh.cn/news/46917f1076104.html)**|   | 文本分析、机器学习、效率专题、生存分析等 |
| 研究设计 | 连玉君    | [我的特斯拉-实证研究设计](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)，[-幻灯片-](https://gitee.com/arlionn/Live/tree/master/%E6%88%91%E7%9A%84%E7%89%B9%E6%96%AF%E6%8B%89-%E5%AE%9E%E8%AF%81%E7%A0%94%E7%A9%B6%E8%AE%BE%E8%AE%A1-%E8%BF%9E%E7%8E%89%E5%90%9B)|
| 面板模型 | 连玉君    | [动态面板模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)，[-幻灯片-](https://quqi.gblhgk.com/s/880197/o7tDK5tHd0YOlYJl)   |
| 面板模型 | 连玉君  | [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) [免费公开课，2小时]  |

- Note: 部分课程的资料，PPT 等可以前往 [连享会-直播课](https://gitee.com/arlionn/Live) 主页查看，下载。

&emsp; 

> #### &#x26F3; [课程主页](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom01.png)](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom02.png)](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom03.png)](https://www.lianxh.cn/news/46917f1076104.html)

> #### &#x26F3; [课程主页](https://www.lianxh.cn/news/46917f1076104.html)

&emsp;

>### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，400+ 推文，实证分析不再抓狂。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- **公众号关键词搜索/回复** 功能已经上线。大家可以在公众号左下角点击键盘图标，输入简要关键词，以便快速呈现历史推文，获取工具软件和数据下载。常见关键词：`课程, 直播, 视频, 客服, 模型设定, 研究设计, stata, plus, 绘图, 编程, 面板, 论文重现, 可视化, RDD, DID, PSM, 合成控制法` 等

&emsp; 

> 连享会小程序：扫一扫，看推文，看视频……

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会小程序二维码180.png)](https://www.lianxh.cn/news/46917f1076104.html)

&emsp; 

> 扫码加入连享会微信群，提问交流更方便

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-学习交流微信群001-150.jpg)

> &#x270F;  连享会-常见问题解答：  
> &#x2728;  <https://gitee.com/lianxh/Course/wikis>  

&emsp; 

> <font color=red>New！</font> **`lianxh` 命令发布了：**    
> 随时搜索连享会推文、Stata 资源，安装命令如下：  
> &emsp; `. ssc install lianxh`  
> 使用详情参见帮助文件 (有惊喜)：   
> &emsp; `. help lianxh`



