[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom01.png)](https://www.lianxh.cn/news/46917f1076104.html)

# 连享会·公开课 【Stata 33 讲】

&#x1F34E; 本课程是 [Stata暑期班](https://gitee.com/arlionn/PX) 的先导课。

> &#x1F449; 点击右上角的【**Fork**】按钮，可以把这个项目完整复制到你的码云账号下，随时查看。 

> &#x1F34E; **课程主页：** https://gitee.com/arlionn/stata101

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

> &emsp;  &emsp; &emsp;  [连享会主页](https://www.lianxh.cn/news/46917f1076104.html) &emsp; | &emsp;  [Stata33讲-推文](https://gitee.com/arlionn/stata101/wikis/B1-Stata%E7%AE%80%E4%BB%8B.md?sort_id=3425452) &emsp; | &emsp; [课程目录](https://gitee.com/arlionn/stata101/wikis/A1_LIst.md?sort_id=2233691)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

## 1. 观看 Stata 33 讲视频

> 温馨提示：请用手机注册账号后观看

- **途径 1：** 连享会直播间
  - http://lianxh.duanshu.com &rarr; 点击【连享会公开课】
  - 亦可 [点击直达网址](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) &rarr; 点击【课程表】
- **途径 2：连享会·b 站**
  - &#x1F449; [前往观看](https://space.bilibili.com/546535876?from=search&seid=1089390775056006818)

## 2. Stata 课件：【LY_Stata.zip】

如果电脑中尚未安装如下外部命令，可以先执行如下代码安装之：
```stata
  ssc install cnssc
  cnssc install dirtools
  cnssc install lianxh, replace
  cnssc install ihelp 
```

- **下载：** [点击下载](https://gitee.com/arlionn/stata101/blob/master/LY_stata.zip)
- **使用：** 解压「**LY_stata.zip**」，放置于 D 盘根目录下即可，即「**D:/LY_stata**」
- **打开 do 文档**：依次在命令窗口输入如下命令即可
   ```stata
   cd D:/LY_stata
   *-浏览所有 dofiles
   ldo 
   *-或打开特定 dofile
   doedit B1_intro.do
   ```

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20210707172434.png)

## 3. 问题和解答

学习过程中，遇到问题或不清楚某些命令的含义，有如下几个途径：
- 可以使用 `help` 命令查看帮助文件，如 `help tabstat`，`help lianxh`
- 可以使用 `ihelp` 命令查看 PDF 帮助文件，解释更细致，也提供了很多范例，如 `ihelp tabstat`, `ihelp xtreg`.
- 强烈建议使用 `lianxh` 和 `songbl` 命令搜索关键词，如：
   ```stata
   . lianxh DID
   . lianxh 面板数据
   . songbl 数据处理
   . songbl 离群值
   ```
- 你也可以扫码加入连享会学员讨论群，提问交流更方便
   
   ![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/%E8%BF%9E%E4%BA%AB%E4%BC%9A-%E5%AD%A6%E4%B9%A0%E4%BA%A4%E6%B5%81%E5%BE%AE%E4%BF%A1%E7%BE%A4001-150.jpg) 

&emsp;

## 4. 附：课程目录

### B. Stata 简介
- **B1** Stata简介
- **B2** Stata文件路径
- **B3** stata命令的语法格式
- **B4** 帮助文件和外部命令
- **B5** stata中的变量名称
- **B6** do文档
- **B7** stata中的函数功能
### D. 数据处理
- **D1** 数据的导入与导出 [已认领-冷萱] 
- **D2** 资料的合并与追加
- **D3** 缺漏值的处理
- **D4** 离群值的处理
- **D5** egen提供的函数
- **D6** 类别变量和分组统计
- **D7** 赫芬达尔指数
### Stata 绘图
- **G1** Stata绘图概览
- **G2** 折线图和连线图
- **G3** 直方图
- **G4** 函数图
### Stata 程序 
- **P1** 单值
- **P2** 暂元
- **P3** 循环语句
- **P4** ado文档：定义自己的程序
- **P5** 随机抽样
### 回归分析
- **R1** OLS简介
- **R2** 相关系数矩阵
- **R3** 基本统计量的呈现
- **R4** 组间均值差异检验
- **R5** 统计表格输出为Excel或Word
- **R6** 拟合值和残差
- **R7** 稳健型标准误的获取
- **R8** 平方项-特殊的交叉项
- **R9** 回归结果的呈现和输出
### 面板数据模型
- **XT1** 面板简介
- **XT2** FE 和 RE 模型
- **XT3** Hausman 检验


&emsp;



&emsp;
 
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

> Stata连享会 &ensp; [主页](https://www.lianxh.cn/news/46917f1076104.html) || [视频](http://lianxh.duanshu.com) || [推文](https://www.lianxh.cn/news/d4d5cd7220bc7.html) || [知乎](https://www.zhihu.com/people/arlionn/) || [Bilibili 站](https://space.bilibili.com/546535876)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-草料主页-一码平川600.png)

&emsp;

## 1. 连享会课程

> **免费公开课：**    
- [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟，[课程主页](https://gitee.com/arlionn/PanelData)，[B 站版](https://www.bilibili.com/video/BV1oU4y187qY)
- [Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. [B 站版](https://space.bilibili.com/546535876/channel/detail?cid=160748)   
- [Stata 小白的取经之路](https://lianxh.duanshu.com/#/brief/course/137d1b7c7c0045e682d3cf0cb2711530) - 龙志能，时长：2 小时，[课程主页](https://gitee.com/arlionn/StataBin) 
- 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等) 

&emsp;

> ### &#x26F3; [课程主页](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom01.png)](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom02.png)](https://www.lianxh.cn/news/46917f1076104.html)

[![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom03.png)](https://www.lianxh.cn/news/46917f1076104.html)

> ### &#x26F3; [课程主页](https://www.lianxh.cn/news/46917f1076104.html)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp;

## 2. 资源分享

### 视频公开课

- [连享会码云：100多个精选计量项目](https://www.lianxh.cn/news/944a69d75cec9.html) |  [新浪视频](https://weibo.com/tv/show/1034:4479228373303338)
- [五分钟 Markdown / Markdown 幻灯片](https://gitee.com/arlionn/md) | [新浪视频](https://weibo.com/tv/show/1034:4484204327796746)
- [连老师给你的-听课建议](https://www.lianxh.cn/news/69706e871c9ad.html)
- [直击面板数据模型](http://lianxh-pc.duanshu.com/course/detail/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟，[B 站版](https://www.bilibili.com/video/BV1oU4y187qY)
- [Stata 33 讲](http://lianxh-pc.duanshu.com/course/detail/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. [课程主页](https://gitee.com/arlionn/stata101)，[课件](https://gitee.com/arlionn/stata101)，[B 站版](https://space.bilibili.com/546535876/channel/detail?cid=160748)    
- [Stata小白的取经之路](https://gitee.com/arlionn/StataBin)，龙志能，上海财经大学，[去听课](https://lianxh.duanshu.com/#/brief/course/137d1b7c7c0045e682d3cf0cb2711530) 

### Stata

- [连享会推文](https://www.lianxh.cn) | [直播视频](http://lianxh.duanshu.com)
- **计量专题课程**: [Stata暑期班/寒假班](https://gitee.com/lianxh/text) | [专题课程](https://gitee.com/arlionn/Course)
- Stata专栏：[最新推文](https://www.lianxh.cn) | [知乎](https://www.zhihu.com/people/arlionn/) | [CSDN](https://blog.csdn.net/arlionn) | [Bilibili 站](https://space.bilibili.com/546535876)
- Books and Journal: [计量Books](https://quqi.gblhgk.com/s/880197/hmpmu2ylAcvHnXwY) | [SJ-PDF](https://quqi.gblhgk.com/s/880197/eipgoUi6Gd1FDZRu) | [Stata Journal-在线浏览](https://www.lianxh.cn/news/12ffe67d8d8fb.html)
- Stata Guys：[Ben Jann](http://www.soz.unibe.ch/about_us/personen/prof_dr_jann_ben/index_eng.html) 

### Data
- [CSMAR-国泰安](http://www.gtarsc.com/#/datacenter/singletable) | [Wind-万德](https://www.wind.com.cn/Default.html) | [Resset-锐思](http://www.resset.cn/databases)
- [常用数据库](https://www.lianxh.cn/news/0b65fd5165c2c.html) 
- [人文社科开放数据库](https://www.lianxh.cn/news/6f06c914acde8.html) 
- [徐现祥教授-IRE-官员交流、方言等](https://www.lianxh.cn/news/8c9f81a5f19ee.html)
- [知乎-Data](https://www.zhihu.com/question/20179699/answer/681756635)

### Papers - 学术论文复现
- [论文重现网站](https://www.lianxh.cn/news/e87e5976686d5.html)
- [Google学术](https://ac.scmor.com/) | [统一入口：虫部落学术搜索](http://scholar.chongbuluo.com/) | [微软学术](https://academic.microsoft.com/home)
- [iData - 期刊论文下载](https://www.cn-ki.net/)
- [ CNKI ](http://scholar.cnki.net/) | [百度学术](http://xueshu.baidu.com/) | [Google学术](https://scholar.glgoo.org/) | [Sci-hub ](http://www.sci-hub.cc/), [2](http://sci-hub.ac/), [3](http://sci-hub.bz/), [4](http://sci-hub.ac/)
- Stata论文重现:  [Harvard dataverse][harvd] | [JFE][jfe]  | [github][git1] | [Yahoo-github][yahoogit]
- 学者主页(提供了诸多论文的原始数据和 dofiles)：[Angrist][Ang1] || [Daron Acemoglu][acem]  || [Ross Levine][ross] || [Esther Duflo][Duflo] || [Imbens](https://scholar.harvard.edu/imbens/software)  ||  [Raj Chetty](http://www.rajchetty.com/)

[harvd]:https://dataverse.harvard.edu/dataverse
[jfe]:http://jfe.rochester.edu/data.htm
[Ang1]:http://economics.mit.edu/faculty/angrist/data1/data
[acem]:http://economics.mit.edu/faculty/acemoglu/data
[ross]:http://faculty.haas.berkeley.edu/ross_levine/papers.htm
[duflo]:http://economics.mit.edu/faculty/eduflo/papers
[git1]:https://github.com/search?utf8=%E2%9C%93&q=stata&type=

[yahoogit]:https://search.yahoo.com/search;_ylt=AwrBT8di2LBZqyEAuG9XNyoA;_ylc=X1MDMjc2NjY3OQRfcgMyBGZyA3lmcC10LTQ3MwRncHJpZAMEbl9yc2x0AzAEbl9zdWdnAzAEb3JpZ2luA3NlYXJjaC55YWhvby5jb20EcG9zAzAEcHFzdHIDBHBxc3RybAMwBHFzdHJsAzE0BHF1ZXJ5A3N0YXRhJTIwZ2l0aHViBHRfc3RtcAMxNTA0NzYxODcz?p=stata+github&fr2=sb-top&fr=yfp-t-473&fp=1

&emsp;

---
>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，500+ 推文，实证分析不再抓狂；[Bilibili 站](https://space.bilibili.com/546535876) 有视频大餐。

&emsp; 

> &#x26F3;  **`lianxh` 命令发布了：**    
> 随时搜索连享会推文、Stata 资源，安装命令如下：  
> &emsp; `. ssc install lianxh`  
> 使用详情参见帮助文件 (有惊喜)：   
> &emsp; `. help lianxh`


&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/横条-远山03-窄版.jpg)

&emsp;


